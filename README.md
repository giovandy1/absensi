# Rancang sistem monitoring absensi berbasis face recongnition dan data reimburstment pada PT. IP1 
# Rancang sistem pengolahan data service dan pembelanjaan serta pejalanan dinas pada PT. IP1

# Rancangan aplikasi 
Aplikasi ini dirancang guna memanage data karyawan pada PT. IP1

# Fitur
Aplikasi ini memiliki beberapa fitur, yaitu sebagai berikut:
1. memiliki 3 akses pengguna yaitu karyawan, hrd, manajemen
2. fitur yang terdapat pada karyawan : 
   - absensi menggunakan face recognition serta geo location 
   - sistem kehadiran, serta pengajuan cuti
   - pengolahan data perjalanan dinas 
   - pengolahan data purchase request 
   - serta medical reimburstment
3. fitur yang terdapat pada hrd :
   - dapat melakukan penambahan karyawan dan data informasi karyawan
   - dapat melakukan pengolahan data terhadap kehadiran karyawan
   - dapat melakukan pengolahan data perjalanan dinas yang di ajukan oleh karyawan 
   - dapat melakukan pengolahan data terhadap uang transport karyawan
   - dapat melakukan pengolahan data terhadap medical reimburstmen karyawan
4. fitur yang terdapat pada manajemen :
   - dapat melakukan pengolahan data serta persetujuan terhadap perjalanan dinas yang diajukan karyawan
   - dapat melakukan pengolahan data serta persetujuan terhadap medical reimburstmen yang diajukan karyawan
   - dapat melakukan pengolahan data serta persetujuan terhadap uang transport yang diajukan karyawan
   - dapat melakukan pengolahan data serta persetujuan terhadap purchase request yang diajukan karyawan

# Build with
aplikasi ini dibangun menggunakan 
* [Codeigniter V3] (https://codeigniter.com/) - framework pada backend
* [Xampp] (https://www.apachefriends.org/index.html) - web server apache
* [Visual Studio Code] (https://code.visualstudio.com/) - Text Editor
* [Bootstrap] (https://getbootstrap.com/) - Markup atau framework pada frontend
* [Open CV] (https://raw.githubusercontent.com/pyannote/pyannote-data/master/openface.nn4.small2.v1.t7) - Library Face Recognition
* [Dompdf] (composer autoload dompdf/dopdf) - Library Pdf

