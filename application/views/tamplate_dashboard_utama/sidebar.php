<!-- Slider Area -->
<section class="home-slider">
			<div class="slider-active">
				<!-- Single Slider -->
				<div class="single-slider overlay">
					<div class="slider-image" style="background-image:url('logo/ip1.jpg')"></div>
					<div class="container">
						<div class="row">
							<div class="col-lg-7 col-md-10 col-12">
								<!-- Slider Content -->
								<div class="slider-content">
									
									<!--/ End Button -->
								</div>
								<!--/ End Slider Content -->
							</div>
						</div>
					</div>
				</div>
				<!--/ End Single Slider -->
			</div>
</section>

<section>
	<div class="container">
		<h3 class="text-center mt-3 text-danger mb-2"><i class="fa fa-map-marker" aria-hidden="true"></i> Lokasi Kantor</h3>
		<div class="embed-responsive embed-responsive-16by9 mb-5">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15865.207613525257!2d106.8077596!3d-6.2238654!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4bc058d6e44133d1!2sIP1%20Jakarta!5e0!3m2!1sid!2sid!4v1624105908377!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
		</div>
	</div>
</section>
		<!--/ End Slider Area -->