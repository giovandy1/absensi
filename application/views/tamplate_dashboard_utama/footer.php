<footer class="footer section">
			<!-- Footer Top -->
			<div class="footer-top overlay">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-6 col-12">
							<!-- About -->
							<div class="single-widget about">
								<h2>ALAMAT</h2>
								<ul class="list">
									<li><i class="fa fa-phone"></i>Phone: (+62 21) 5260808 </li>
									<li><i class="fa fa-phone"></i>Fax: (+62 21) 5266006</li>
									<li><i class="fa fa-envelope"></i>Email: info@ip1.co.id <a href="info@ip1.co.id">info@ip1.co.id</a></li>
									<li><i class="fa fa-map-o"></i>ALAMAT :Menara Mandiri II, Lt. 18. Jl. Jenderal Sudirman No. 54-55, Senayan, Kebayoran Baru, Jakarta Selatan 12190.</li>
								</ul>
								<!-- Social -->
								<ul class="social">
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li class="active"><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-youtube"></i></a></li>
								</ul>
								<!-- End Social -->
							</div>
							<!--/ End About -->
						</div>
						<div class="col-lg-4 col-md-6 col-12">
							<!-- Useful Links -->
							<div class="single-widget list">
								<h2>Link terkait</h2>
								<ul>
									<li><i class="fa fa-angle-right"></i><a href="#">WEBSITE PT Citra Agung Mestika</a></li>
									<li><i class="fa fa-angle-right"></i><a href="http://ip1.co.id/">Ip1.coid</a></li>
								</ul>
							</div>
							<!--/ End Useful Links -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Footer Top -->
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<!-- Copyright -->
							<div class="copyright">
								<p>© Copyright PT Citra Agung Mestika 2020.</p>
							</div>
							<!--/ End Copyright -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Footer Bottom -->
		</footer>
		<!--/ End Footer -->